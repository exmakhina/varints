#!/usr/bin/env python
# LEB128 varint encoding/decoding
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-varint@zougloub.eu> & contributors
# SPDX-License-Identifier: CC-PDDC

def dump(n, fp):
	while True:
		limb = n & 0x7f
		n >>= 7
		if n:
			fp.write((limb | 0x80).to_bytes())
		else:
			fp.write(limb.to_bytes())
			break

def dumps(n):
	buf = bytearray()
	while True:
		limb = n & 0x7f
		n >>= 7
		if n:
			buf.append(limb | 0x80)
		else:
			buf.append(limb)
			break
	return buf

def load(fp):
	shift = 0
	n = 0
	while True:
		b = fp.read(1)
		if not b:
			raise EOFError()
		i = b[0]
		n |= (i & 0x7f) << shift
		shift += 7
		if not (i & 0x80):
			break
	return n

def loads(buf):
	i = 0
	n = 0
	shift = 0
	while True:
		b = buf[i]
		n |= (b & 0x7f) << shift
		shift += 7
		if b & 0x80 == 0:
			break
		i += 1
	return n
