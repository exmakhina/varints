#!/usr/bin/env python
# Exotic varint encoding/decoding test
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-varint@zougloub.eu> & contributors
# SPDX-License-Identifier: CC-PDDC

import logging

from hypothesis import strategies as st
from hypothesis import given


from . import fss_ts


logger = logging.getLogger(__name__)


@given(i=st.integers(min_value=0, max_value=fss_ts.max_value))
def test_s(i):
	v = fss_ts.dumps(i)
	logger.info("%30d %s", i, " ".join(f"{_:08b}" for _ in v))
	k = fss_ts.loads(v)
	assert k == i, f"Wanted {i} got {k}"
