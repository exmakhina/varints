#!/usr/bin/env python
# Example varint encoding/decoding
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-varint@zougloub.eu> & contributors
# SPDX-License-Identifier: CC-PDDC

"""
Properties:

- Doesn't encode to NUL-bytes
- Preserves lexicographic ordering
- Optimized for large numbers at the expense of small ones
- Slow

"""

import logging


from . import base2base


logger = logging.getLogger(__name__)


max_value = (255**12)-1

steps = [
 0,
 (255 ** 7),
 (255 ** 7) * 0b00111111 + (255 ** 7) - 1,
 (255 ** 7) * 0b00111111 + (255 ** 7),
 (255 ** 8)-1,
 (255 ** 8),
 (255 ** 8) * 0b00011111 + (255 ** 8) - 1,
 (255 ** 8) * 0b00011111 + (255 ** 8),
 (255 ** 9)-1,
 (255 ** 9),
 (255 ** 9) * 0b00001111 + (255 ** 9) - 1,
 (255 ** 9) * 0b00001111 + (255 ** 9),
 (255 ** 10)-1,
 (255 ** 10),
]
for i in range(80):
	steps.append((1<<i)-1)
	steps.append((1<<i))

steps = sorted(steps)

def dumps(n: int) -> bytes:
	limbs = [(1+_) for _ in base2base.bigint2seq_be(n, 255)]

	for i in range(6):
		n = 0b00111111 >> i
		f = ~n
		m = f & (0xff - (1<<(8-i-2)))
		l = 8 + i

		if l == 8:
			assert n == 0b00111111, f"Got {n=:08b}"
			assert m == 0b10000000, f"Got {m=:08b}"

		while len(limbs) < l:
			limbs = [1] + limbs

		if len(limbs) == l and (limbs[0] & f) == 0:
			limbs[0] |= m
			return bytes(limbs)


def loads(e: bytes) -> int:
	first = e[0]

	e = bytearray(e)

	for i in range(6):
		n = 0b00111111 >> i
		m = ~n
		m &= 0xff - (1<<(8-i-2))
		l = 8 + i

		if (first & (1<<(8-i-2))) == 0:
			e[0] &= n
			e = [(_-1) for _ in e]
			return base2base.seq2bigint_be(e[:l], 255)
