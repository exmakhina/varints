##############################
Variable Integer Encodings Zoo
##############################


This repo contains some info about variable integer encodings that can
be seen in the wild.


This README has license `CC BY-NC-SA 4.0 <https://creativecommons.org/licenses/by-nc-sa/4.0/>`_.


Zoo
###

VLQ
***

Base-128 representation of an unsigned integer with the addition of
the eighth bit to mark continuation of bytes; big endian.

::

   > 0xxxxxxx
   > 1xxxxxxx 0xxxxxxx


:Reference: https://en.wikipedia.org/wiki/Variable-length_quantity

:Complexity: simple


:Pros: simple

:Cons:

 - Possibility of NUL byte in message.
 - Doesn't preserve lexicographic ordering
 - O(n) length determination


LEB128
******

Like VLQ but little-endian.

::

   < 0xxxxxxx
   < 1xxxxxxx 0xxxxxxx

:Reference: https://en.wikipedia.org/wiki/LEB128

:Complexity: simpler

:Pros: simple, arguably better than VLQ_.

:Cons:

 - Can have NUL bytes... but only for int(0)
 - Doesn't preserve lexicographic ordering
 - O(n) length determination


VLQ alternative
***************


::

   > 1xxxxxxx
   > 0xxxxxxx 1xxxxxxx
   > 0xxxxxxx 0xxxxxxx 1xxxxxxx

:Complexity: simple

:Pros: simple

:Cons:

 - Can contain NUL bytes
 - Doesn't preserve lexicographic ordering
 - O(n) length determination


FSS-UTF Proposal (early)
************************

The length is known in advance from the first byte.

:Reference: https://en.wikipedia.org/wiki/UTF-8#FSS-UTF

::

   > 0xxxxxxx
   > 10xxxxxx 1xxxxxxx
   > 110xxxxx 1xxxxxxx 1xxxxxxx
   > 1110xxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx
   > 11110xxx 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx


:Complexity: simple

:Pros:

 - Simple
 - Preserves lexicographic ordering
 - O(1) length determination


UTF-8
*****

Evolution of FSS-UTF.

:Reference: https://en.wikipedia.org/wiki/UTF-8#FSS-UTF

::

   > 0xxxxxxx
   > 110xxxxx 10xxxxxx
   > 1110xxxx 10xxxxxx 10xxxxxx
   > 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
   > 111110xx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
   > 1111110x 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx

:Pros:

 - Simple
 - Preserves lexicographic ordering
 - O(1) length determination


FSS-Likes
*********


::

   > 0xxxxxxx
   > 10xxxxxx 1xxxxxxx
   > 110xxxxx 1xxxxxxx 1xxxxxxx
   > 1110xxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx
   > 11110xxx 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx
   > 111110xx 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx
   > 1111110x 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx
   > 11111110 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx


::

   > 0xxxxxxx
   > 10xxxxxx 1xxxxxxx
   > 110xxxxx 1xxxxxxx 1xxxxxxx
   > 1110xxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx
   > 11110xxx 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx
   > 111110xx 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx
   > 1111110x 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx
   > 11111110 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx 1xxxxxxx

   > # Bigint extension using...
   > 11111111 10yyyyyy 1xxxxxxx * Y
   > 11111111 110yyyyy 1yyyyyyy 1xxxxxxx * Y
   > 11111111 1110yyyy 1yyyyyyy 1yyyyyyy 1xxxxxxx * Y


:Complexity: simple

:Pros:

 - Preserves lexicographic ordering
 - O(1) length determination


PrefixVarint
************

The first byte contains a prefix indicating the length;
the prefix is in the least significant bits of the byte,
so CTZ instruction can be used.

:Reference: https://web.archive.org/web/20201119135834/:Reference: https://github.com/stoklund/varint#prefixvarint

::

   > xxxxxxx1  7 bits in 1 byte
   > xxxxxx10 14 bits in 2 bytes
   > xxxxx100 21 bits in 3 bytes
   > xxxx1000 28 bits in 4 bytes
   > xxx10000 35 bits in 5 bytes
   > xx100000 42 bits in 6 bytes
   > x1000000 49 bits in 7 bytes
   > 10000000 56 bits in 8 bytes
   > 00000000 64 bits in 9 bytes

:Complexity: simple.

:Pros:

 - Use ctz instruction
 - O(1) length determination

:Cons:

 - Doesn't preserve lexicographic ordering


SQLite
******

The first byte holds a value or is a prefix
indicating length, if more bytes.

::

   *--- bytes

        *-- max integer

                *-- digits represented

   1 	240 	2.3
   2 	2287 	3.3
   3 	67823 	4.8
   4 	224-1 	7.2
   5 	232-1 	9.6
   6 	240-1 	12.0
   7 	248-1 	14.4
   8 	256-1 	16.8
   9 	264-1 	19.2


:Reference: https://sqlite.org/src4/doc/trunk/www/varint.wiki

:Complexity: medium.

:Pros:

 - Preserves lexicographic ordering
 - O(1) length determination
 - Rather tight for tiny or large values

:Cons:

 - Can contain NUL bytes
 - Rather less tight for values between 2048 and 33.554e6


Dlugosz' Variable-Length Integer Encoding — Revision 2
******************************************************

:Reference: https://web.archive.org/web/20150812060227/http://www.dlugosz.com/ZIP2/VLI.html
            (2003 by John M. Dlugosz)

::

  >
  prefix bits	bytes	data bits	unsigned range
  0             1         7    127
  10            2        14    16,383
  110           3        21    2,097,151

  111 00        4        27    134,217,727 (128K)
  111 01        5        35    34,359,738,368 (32G)
  111 10        8        59    holds the significant part of a Win32 FILETIME
  111 11 000    6        40    1,099,511,627,776 (1T)
  111 11 001    9        64    A full 64-bit value with one byte overhead
  111 11 010    17      128    A GUID/UUID
  111 11 111    n       any    Any multi-precision integer

:Complexity: simple/medium

:Pros:

 - Preserves lexicographic ordering
 - O(1) length determination

:Cons:

 - Wasted values


CBOR
****

Prefix in MSbs of first byte, acting as type AND length indicator;
length can be 1, 1+1, 1+2, 1+4, 1+8, or more with bigint.

::

   TODO

:Reference: https://www.rfc-editor.org/rfc/rfc8949.html

:Complexity: simplest

:Pros:

 - Preserves lexicographic ordering
 - O(1) length determination
 - One solution fits all (for other kinds of data)

:Cons:

 - Since CBOR represents other information than integers,
   not as tight, in particular there's no encoding that gives
   a 4-byte message (for ~ u24).

 - Wasted values (space-time tradeoff); the same value can be
   represented in 1+(0,1,2,4,8,N) bytes encoding (the spec
   says an encoder should use the most compact encoding).


Imperial Varint
***************

Prefix-based, leading zeros indicate number of extra bytes, big endian.

:Reference: https://dcreager.net/2021/03/a-better-varint/

:Complexity: simple

:Pros: simple

:Cons:

 - Can contain NUL bytes
 - Doesn't preserve lexicographic ordering


Bitcoin CompactSize
*******************

Prefix-based, little endian, values before 0xFD are single-byte, then
the first byte value corresponds to 2/4/8 following bytes.

:Reference: https://en.bitcoin.it/wiki/Protocol_documentation#Variable_length_integer
            https://wiki.bitcoinsv.io/index.php/VarInt

:Complexity: simpler

:Pros: simple

:Cons:

 - Can contain NUL bytes
 - Doesn't preserve lexicographic ordering
 - Wasted storage

Note: not to be mistaken with the other bitcoin `VARINT` which is VLQ_.


vu128
*****

Prefix-based, little endian, values before 0xF0 are single-byte, then
the first byte value minus 0xEF is the number of extra bytes.

:Reference: https://john-millikin.com/vu128-efficient-variable-length-integers

:Complexity: simple

:Pros: simple

:Cons:

 - Can contain NUL bytes
 - Doesn't preserve lexicographic ordering

Considers application to floating-point values (which are stored big-endian).


bigint-serializer
*****************

Variant of LEB128_ (little-endian, continuation bit at 0x80)
with storage of sign bit at bit 0 of first byte.

:Reference: https://github.com/michaelficarra/bigint-serialiser

:Complexity: simple

:Pros: simple

:Cons:

 - Can contain NUL bytes
 - Doesn't preserve lexicographic ordering


Considerations
##############


Prefix Construction
*******************

Instead of using continuation bits spread throughout the number,
(as done in VLQ_ or LEB128_), some encodings use a designated area
for storing the information which allows to resolve the size of the
encoding.

In the case of PrefixVarint_, FSS-UTF_, UTF-8_ the prefix is of
limited size, but in other cases it can have an large to unlimited
size, for example an extension of FSS-UTF_ can have base-1 prefixes
extending forever ; CBOR_'s bigint have a prefix `11000010` while
64-bit integers have prefix `00011011`, 32-bit integers `00011010`...

::

  > 000 xxxxx
  > 001 xxxxx xxxxxxxx
  > 010 xxxxx xxxxxxxx xxxxxxxx
  > 011 xxxxx xxxxxxxx xxxxxxxx xxxxxxxx
  > 100 xxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx
  > 101 xxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx
  > 110 xxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx
  > 111 xxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx


Density
*******

Given a distribution of integers, how big is going to be the data?

Note: there is a test in this repo, producing a visualization of the
progression of the encoded size of a varint depending on the number.


Preservation of Lexicographic Ordering
**************************************

Given a comparison function (returning >0, 0 or <0):

.. code:: python

   def cmp(a, b):
       return 1 if a > b else -1 if a < b else 0

A varint can be designed so :code:`cmp(dumps(a), dumps(b))`
produces the same result `cmp(a,b)`.

The construction usually involves to use big endian (so comparison
done byte by byte in reading order will be easy) and to have the size
indicator acting as a prefix, which is increasing as the number
increases (thus increases in size); for example, in this ficticious encoding:

::

   > 0xxxxxxx
   > 1xxxxxxx xxxxxxxx xxxxxxxx

Numbers using the second encoding would always be greater than numbers
using the first encoding.


