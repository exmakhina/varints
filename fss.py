#!/usr/bin/env python
# FSS-UTF varint encoding/decoding
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-varint@zougloub.eu> & contributors
# SPDX-License-Identifier: CC-PDDC

import logging


logger = logging.getLogger(__name__)


def make_table(logger=None):
	"""
	Create variable encoding table for our encoding.
	"""
	varint_table = []
	a = 0
	for more_bytes in range(9):
		nbytes = more_bytes+1
		available_first = 8 - 1 - more_bytes
		if available_first > 0:
			prefix = (0b111111110 << available_first) & 0xff
		else:
			prefix = (0b111111110 >> (-available_first)) & 0xff

		used = more_bytes+1 + more_bytes*1

		if more_bytes == 8:
			used -= 1 # if we really want to cram more range

		free = (8 + more_bytes*8) - used

		b = a + (2**free)-1

		if logger:
			pattern = bin(prefix)[2:][:more_bytes+1].ljust(8, "x")
			for n in range(1, 1+more_bytes):
				pattern += " 1xxxxxxx"

			logger.info("%d %18d %18d used %2d free %2d format %s",
			 more_bytes, a, b, used, free, pattern)

		varint_table.append((a, b, prefix))

		a = b+1

	return varint_table


varint_table = make_table()


def dumps(n: int) -> bytes:
	for more_bytes, (a, b, prefix_first) in enumerate(varint_table):
		# TODO bisect
		if a <= n <= b:
			n -= a
			shift = 7*more_bytes
			enc = bytearray([prefix_first | (n >> shift)])
			for i in range(more_bytes):
				shift -= 7
				enc.append(0x80 | (n >> shift) & 0x7f)
			return enc


def loads(e: bytes) -> int:
	first = e[0]

	for i in range(8):
		if first & (1<<(7-i)) == 0:
			break
	else:
		i = 8

	more_bytes = i

	for more_bytes_, (a, b, prefix_first) in enumerate(varint_table):
		if more_bytes_ != more_bytes:
			continue # TODO bisect

		n = a

		shift = 7*more_bytes

		n += (first & ~prefix_first) << shift
		for i in range(more_bytes):
			shift -= 7
			n += (e[i+1] & ~0x80) << shift
		return n
