#!/usr/bin/env python
# Illustrations for different varint encodings
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-varint@zougloub.eu> & contributors
# SPDX-License-Identifier: CC-PDDC

import logging
import itertools
import math

import pytest

try:
	from hypothesis import given, strategies as st
	HYPOTHESIS_AVAILABLE = True
except ImportError:
	HYPOTHESIS_AVAILABLE = False


logger = logging.getLogger(__name__)


@pytest.fixture
def available_encoders():
	methods = list()


	def try_leb128():
		try:
			from .leb128 import dumps
		except:
			return []

		values = []
		for limb in range(12):
			v = (1<<(7*limb))-1
			values.append(v)
			values.append(v+1)
		return [("LEB128", values, dumps)]

	methods += try_leb128()


	def try_beb128():
		try:
			from .beb128 import dumps
		except:
			return []

		values = []
		for limb in range(12):
			v = (1<<(7*limb))-1
			values.append(v)
			values.append(v+1)
		return [("BEB128", values, dumps)]

	methods += try_beb128()


	def try_fss():
		try:
			from .fss import make_table, dumps
		except:
			return []

		values = []
		for a, b, *rem in make_table():
			values.append(a)
			values.append(b)
		return [("FSS", values, dumps)]

	methods += try_fss()


	def try_cbor():
		try:
			from cbor2 import dumps
			#from exmakhina.cbor import dumps
		except:
			logger.info("cbor2 module not available")
			return []

		values = set()
		for i in range(256):
			values.add(i)

		for size in [1,2,4,8]:
			v = (1<<(8*size))-1
			values.add(v)

			if size < 8:
				values.add(v+1)

		return [("CBOR", sorted(values), dumps)]

	methods += try_cbor()


	def try_sqlite():
		try:
			import varints.sqliteu.sqliteu as sqlite
			#from exmakhina.cbor import dumps
		except:
			logger.info("cbor2 module not available")
			return []

		values = [0,
		 sqlite.ONE_BYTE_LIMIT, sqlite.ONE_BYTE_LIMIT+1,
		 sqlite.TWO_BYTE_LIMIT, sqlite.TWO_BYTE_LIMIT+1,
		 sqlite.THREE_BYTE_LIMIT, sqlite.THREE_BYTE_LIMIT+1,
		 sqlite.FOUR_BYTE_LIMIT, sqlite.FOUR_BYTE_LIMIT+1,
		 sqlite.FIVE_BYTE_LIMIT, sqlite.FIVE_BYTE_LIMIT+1,
		 sqlite.SIX_BYTE_LIMIT, sqlite.SIX_BYTE_LIMIT+1,
		 sqlite.SEVEN_BYTE_LIMIT, sqlite.SEVEN_BYTE_LIMIT+1,
		 sqlite.EIGHT_BYTE_LIMIT, sqlite.EIGHT_BYTE_LIMIT+1,
		 sqlite.NINE_BYTE_LIMIT,
		]

		return [("SQLite", sorted(values), sqlite.encode)]

	methods += try_sqlite()


	def try_fss_ts():
		try:
			from . import fss_ts
		except:
			return []

		return [("FSS-TS", fss_ts.steps, fss_ts.dumps)]

	methods += try_fss_ts()

	yield methods


@pytest.mark.skipif(not HYPOTHESIS_AVAILABLE, reason="Hypothesis is not available")
def test_lexicographic_ordering_preservation(available_encoders):
	"""
	"""
	def cmp(x, y):
		if x < y: return -1
		elif x > y: return 1
		else: return 0

	for name, values, dumps in available_encoders:

		@given(
		 i=st.integers(min_value=0, max_value=values[-1]),
		 j=st.integers(min_value=0, max_value=values[-1]),
		)
		def test(i, j):
			assert cmp(i,j) == cmp(dumps(i), dumps(j))

		try:
			test()
			logger.info("%s: preserves lexicographic ordering", name)
		except Exception as e:
			logger.warning("%s: does not preserve lexicographic ordering: %s", name, e)


def test_encodings_size_progression(available_encoders):
	"""
	Produce plot showing encoded size of varints
	"""

	logging.getLogger("matplotlib.font_manager").setLevel(logging.WARNING)

	import matplotlib.figure
	import matplotlib.path
	import matplotlib.patches
	import matplotlib.backend_bases

	subpcfg = matplotlib.figure.SubplotParams(
	 left  =0.10,
	 bottom=0.10,
	 right =0.90,
	 top   =0.90,
	 wspace=0.00,
	 hspace=0.00,
	)
	figure = matplotlib.figure.Figure(
	 facecolor='white',
	 edgecolor='white',
	 subplotpars=subpcfg,
	)

	figure.set_figheight(8)
	figure.set_figwidth(12)
	figure.set_dpi(72)

	axes = figure.add_subplot(1, 1, 1)

	axes.xaxis.grid(True)
	axes.yaxis.grid(True)
	axes.set_xlabel("Encoded number")
	axes.set_ylabel("Encoded size (B)")

	x_max = 0
	y_max = 0
	for name, values, dumps in available_encoders:
		xs = []
		ys = []
		for x in values:
			if x == 0:
				xl = -1
			else:
				xl = math.log2(x)
			y = len(dumps(x))
			xs.append(xl)
			ys.append(y)

			y_max = max(y_max, y)
			x_max = max(x_max, x)

		logger.info("- %s", name)
		logger.info("  xs = %s", xs)
		logger.info("  ys = %s", ys)
		axes.plot(xs, ys, label=name)

	axes.set_yticks(range(y_max+1))
	xticks = []
	xticks_minor = []
	xticklabels = []
	xticklabels_minor = []
	for shift in itertools.count():
		xl = shift
		if (1<<xl) > x_max:
			break
		if shift % 4 == 0:
			xticks.append(xl)
			xticklabels.append(f"2^{shift}")
		else:
			xticks_minor.append(xl)
			xticklabels_minor.append("")
	axes.set_xticks(xticks)
	axes.set_xticks(xticks_minor, minor=True)
	axes.set_xticklabels(xticklabels)
	axes.set_xticklabels(xticklabels_minor, minor=True)


	polygon_coords = [(-1, 1)]
	for b in range(1,12):
		x = math.log2((1<<(8*b))-1)
		y = b
		polygon_coords.append((x,y))
		x = math.log2((1<<(8*b)))
		y = b+1
		polygon_coords.append((x,y))

	polygon_coords.append((polygon_coords[-1][0],0))
	polygon_coords.append((polygon_coords[0][0],0))
	polygon_coords.append(polygon_coords[0])

	polygon_path = matplotlib.path.Path(polygon_coords,
	 closed=True,
	)
	polygon = matplotlib.patches.PathPatch(polygon_path,
	 facecolor='none',
	 edgecolor=(1, 0, 0, 0.25),
	 hatch='//',
	)

	axes.add_patch(polygon)

	axes.legend()

	title = f"Varint Encoding Density"
	figure.suptitle(title)

	out_dir = "."
	base = "test"
	for ext in ("png",):
		canvas_class = matplotlib.backend_bases.get_registered_canvas_class(ext)
		figure_canvas = canvas_class(figure)
		canvas_print = getattr(figure_canvas, 'print_%s' % ext)
		canvas_print("%s.%s" % (base, ext))
