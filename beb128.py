#!/usr/bin/env python
# VLQ varint encoding/decoding
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-varint@zougloub.eu> & contributors
# SPDX-License-Identifier: CC-PDDC

def dumps(n):
	if n == 0:
		return bytes([0])

	parts = []
	while n > 0:
		parts.append(n & 0x7F)
		n >>= 7

	parts.reverse()

	for i in range(len(parts) - 1):
		parts[i] |= 0x80

	return bytes(parts)

def loads(buf):
	n = 0
	for byte in buf:
		n = (n << 7) | (byte & 0x7F)
		if byte & 0x80 == 0:
			break
	return n

