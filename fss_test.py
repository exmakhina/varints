#!/usr/bin/env python
# FSS-UTF varint encoding/decoding test
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-varint@zougloub.eu> & contributors
# SPDX-License-Identifier: CC-PDDC

import logging

from hypothesis import strategies as st
from hypothesis import given

from . import fss


logger = logging.getLogger(__name__)


@given(i=st.integers(min_value=fss.varint_table[0][0], max_value=fss.varint_table[-1][1]))
def test_s(i):
	v = fss.dumps(i)
	logger.debug("%s", " ".join(f"{_:08b}" for _ in v))
	k = fss.loads(v)
	assert k == i
