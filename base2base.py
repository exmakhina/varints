#!/usr/bin/env python
# coding: utf-8
# Radix conversion experiments
# SPDX-FileCopyrightText: 2021,2024 Jérôme Carretero <cJ-varint@zougloub.eu> & contributors
# SPDX-License-Identifier: CC-PDDC


import logging

import typing as t


logger = logging.getLogger(__name__)



def seq2bigint_be(i: t.Sequence[int], base_i: int) -> int:
	"""
	"""
	n = 0
	for c in i:
		n = n * base_i + c
	return n


def seq2bigint_le(i: t.Sequence[int], base_i: int) -> int:
	"""
	"""
	n = 0
	a = 1
	for c in i:
		n += c * a
		a *= base_i
	return n


def bigint2seq_le(n: int, base_o: int) -> t.Sequence[int]:
	"""
	:param base_o: base of output sequence
	:return: converted sequence, big endian
	"""

	o = list()
	while True:
		n, r = divmod(n, base_o)
		o.append(r)
		if n == 0:
			break

	return o


def bigint2seq_be(n: int, base_o: int) -> t.Sequence[int]:
	"""
	:param base_o: base of output sequence
	:return: converted sequence, big endian
	"""
	no = 1
	a = 1
	factors = []
	while no <= n:
		factors.append(a)
		a *= base_o
		no *= base_o

	factors.reverse()
	logger.debug("Factors: %s", factors)

	o = []
	mod = n
	for factor in factors:
		div, mod = divmod(mod, factor)
		o.append(div)

	return o

