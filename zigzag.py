#!/usr/bin/env python
# Zigzag encoding -- https://en.wikipedia.org/wiki/Variable-length_quantity#Zigzag_encoding
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-varint@zougloub.eu> & contributors
# SPDX-License-Identifier: CC-PDDC

def encode(n):
	return -2*n -1 if n < 0 else 2*n

def decode(n):
	return -n // 2 if n & 1 == 1 else n // 2
