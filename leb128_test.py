#!/usr/bin/env python
# LEB128 varint encoding/decoding test
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-varint@zougloub.eu> & contributors
# SPDX-License-Identifier: CC-PDDC

import io
import logging

from hypothesis import strategies as st
from hypothesis import given


logger = logging.getLogger(__name__)


@given(x=st.integers(min_value=0))
def test_leb128_s(x):
	from .leb128 import (loads, dumps)
	logger.debug("Put %s %s", type(x), x)
	enc = dumps(x)
	logger.debug("Enc: %s", " ".join(f"{_:08b}" for _ in enc))
	dec = loads(enc)
	logger.debug("Got %s %s", type(dec), dec)
	assert dec == x, dec

@given(x=st.integers(min_value=0))
def test_leb128_f(x):
	from .leb128 import (load, dump)
	with io.BytesIO() as fio:
		logger.debug("Put %s %s", type(x), x)
		dump(x, fio)
		logger.debug("Dumped: %s", " ".join(f"{_:08b}" for _ in fio.getvalue()))
		fio.seek(0)
		dec = load(fio)
		logger.debug("Read %s %s", type(dec), dec)
		assert dec == x, dec
