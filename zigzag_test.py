#!/usr/bin/env python
# Zigzag encoding - test
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-varint@zougloub.eu> & contributors
# SPDX-License-Identifier: CC-PDDC

import logging

from hypothesis import strategies as st
from hypothesis import given

from . import zigzag


logger = logging.getLogger(__name__)


@given(n=st.integers())
def test(n):
	logger.debug("Put %s", n)
	e = zigzag.encode(n)
	logger.debug("Enc: %s", e)
	d = zigzag.decode(e)
	logger.debug("Got %s", d)
	assert d == n, d
